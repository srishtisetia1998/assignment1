package org.costcalculation;

import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;




public class CostCalculationApplication {

	public static void main(String args[])throws Exception
	{
		Logger logger = Logger.getLogger(CostCalculationApplication.class.getName());
		
		Options options = new Options();

	    Option propertyOption = Option.builder()
	         .longOpt("1")
	         .argName("property=value" ) 
	         .hasArgs()
	         .valueSeparator()
	         .numberOfArgs(2)
	         .desc("use value for given properties" )
	         .build();

	      options.addOption(propertyOption);

	      CommandLineParser parser = new DefaultParser();
	      CommandLine cmd = parser.parse( options, args);
		
	      if(cmd.hasOption("1")) {
			  
			  try 
			  {
				  	Properties properties = cmd.getOptionProperties("1");
				  	String name=properties.getProperty("name");
				  	float price=Float.parseFloat(properties.getProperty("price")); 
				  	int quantity=Integer.parseInt(properties.getProperty("quantity"));
				  	int type=Integer.parseInt(properties.getProperty("type"));
		
				  	CostCalculationService p=new CostCalculationService(name,price,quantity,type);

				  	logger.log(Level.INFO,"** Final product details are:- ** \n");

				  	p.displayDetails();
				  	final float finalResult[]=p.taxCalculator(type,price,quantity);
	    	  
				  	System.out.println("Product tax per item: "+ finalResult[0]);
				  	System.out.println("Product final price as per quantity: "+ finalResult[1]);
	    	  }
	    	  
	    	  catch(Exception e)
	    	  {
	    		  System.out.println("Exception is:");
	    		  e.printStackTrace();
	    	  }
	      }
	     
	      System.out.println("\nDo you want to add more items \n"+
	    		  				"Press Y if yes or N if not");
		
	      Scanner sc=new Scanner(System.in);
	      char opt=sc.next().charAt(0);
	      sc.nextLine();
		

	      while(opt=='Y' || opt=='y'){
				
			try {
					System.out.print("Enter Product name:");
					String name=sc.nextLine();
	
					System.out.println("Enter Product price:");
					float price=sc.nextFloat();
	
					System.out.println("Enter Product quantity:");
					int quantity=sc.nextInt();
	
					System.out.println("Enter Product type: \n**Enter 1 for RAW**" +
											"\n**Enter 2 for MANUFACTURED**"+ 
												"\n**Enter 3 for IMPORTED** ");
					int type=sc.nextInt();
					CostCalculationService p=new CostCalculationService(name,price,quantity,type);

					logger.log(Level.INFO,"** Final product details are:- ** \n");

			    	p.displayDetails();
			    	final float finalResult[]=p.taxCalculator(type,price,quantity);
			    	  
			    	System.out.println("Product tax per item: "+ finalResult[0]);
			  		System.out.println("Product final price as per quantity: "+ finalResult[1]);
				}
		
			catch(Exception e)
				{	
					System.out.println("Exception is: ");
					e.printStackTrace();
				}

			logger.log(Level.INFO,	"Do you want to add more items \n"+
									"Press Y if yes or N if not");
			opt=sc.next().charAt(0);
			sc.nextLine();
		
		}
		sc.close();
	}
}
