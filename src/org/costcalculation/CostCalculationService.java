package org.costcalculation;

public class CostCalculationService extends Item{
	
	CostCalculationService(String name, float price, int quantity, int type) {
		super(name, price, quantity, type);
	}

	 float[] taxCalculator(int type,float price,int quantity)
	{
		float[] finalResult= {0,0};
		switch(type)
		{
			case 1: return taxCalculatorForRAW(price, quantity);
		
			case 2: return taxCalculatorForMANUFACTURED(price, quantity);
			
			case 3: return taxCalculatorForIMPORTED(price, quantity);
			
			default:System.out.println("Invalid value");
		}
		
		return finalResult;
	}
	
	private float[] taxCalculatorForRAW(float price,int quantity)
	{
		float tax=(float)12.5*price/100;
		float newprice=quantity*(price+tax);
		float finalResult[]= {tax,newprice};
		
		return finalResult;
	}
	
	private float[] taxCalculatorForMANUFACTURED(float price,int quantity)
	{
		float tax=(float)12.5*price/100;
		tax+=(float)2*(price + tax)/100;
		float newprice=quantity*(price+tax);
		float finalResult[]= {tax,newprice};
		
		return finalResult;
	}
	
	private float[] taxCalculatorForIMPORTED(float price,int quantity)
	{
		float tax=(float)10*price/100;
		float newprice=quantity*(price+tax);
		
		newprice=calculateSurcharge(newprice);
		
		float finalResult[]= {tax,newprice};
		return finalResult;
	}
	
	private float calculateSurcharge(float newprice)
	{
		if(newprice<=100)
			newprice+=5;

		else if(newprice>100 && newprice<=200)
			newprice+=10;

		else
			newprice+=20;
		
		return newprice;
	}
}
