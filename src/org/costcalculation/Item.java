package org.costcalculation;

public class Item {

	private final String name;
	private final float price;
	private final int quantity;
	private final int type;
	
	public Item(String name, float price, int quantity, int type) {
		this.name = name;
		this.price = price;
		this.quantity = quantity;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public float getPrice() {
		return price;
	}

	public int getQuantity() {
		return quantity;
	}

	public String getType() {
		
		if(type==1)
			return "RAW";
		else if(type==2)
			return "MANUFACTURED";
		else if(type==3)
			return "IMPORTED";
		else
			return "Invalid Value entered";
	}
	
	public void displayDetails()
	{
		System.out.println("Product name: "+ getName());
		System.out.println("Product price before tax: "+ getPrice());
		System.out.println("Product quantity: "+ getQuantity());
		System.out.println("Product type: "+ getType());
	}
	
}
